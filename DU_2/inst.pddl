(define (problem blocks-3)
    (:domain blocks)
    (:requirements :strips)
    (:objects herec 
les pristav reka hospoda mesto akademie majak more ostrov 
clun drevo zrnko fregata mince karavela
alkohol )

    (:init
        (nachazi herec pristav)
        (je-herec herec)
        (je-les les)
        (je-pristav pristav)
        (je-reka reka)
        (je-hospoda hospoda)
        (je-mesto mesto)
        (je-akademie akademie)
        (je-majak majak)
        (je-more more)
        (je-ostrov ostrov)
        (je-clun clun)
        (je-drevo drevo)
        (je-zrnko zrnko)
        (je-fregata fregata)
        (je-mince mince)
        (je-karavela karavela)
        (je-alkohol alkohol)
    )
    
    (:goal (and
        (vyhral herec)
        )
    )
)
