(define (domain blocks)
    (:requirements :strips)
    (:predicates
(zenat ?x) (admiral ?x)  (kokain ?x) (nachazi ?x ?y) 
 (vnalade ?x) (opily ?x) (zavisly ?x) (cast-sveta ?x)
 (je-herec ?x) (je-les ?x) (je-pristav ?x) (je-reka ?x) (je-hospoda ?x) (je-mesto ?x) (je-akademie ?x) (je-majak ?x) (je-more ?x)
 (je-ostrov ?x) (je-clun ?x) (je-drevo ?x) (je-zrnko ?x) (je-fregata ?x) (je-mince ?x) (je-karavela ?x) (je-alkohol ?x)
 (ma-alkohol ?x) (ma-mapku ?x)
 (ma-drevo ?x) (ma-fregatu ?x) (ma-karavelu ?x) (ma-clun ?x) (ma-zrnko ?x) (ma-mince ?x) (ma-cihlu ?x) (ma-perlu ?who)
 (je-v-tr-rejstriku ?x) (porvan-z-mevedem ?x) (byl-v-bitce ?x) (porazil-piraty ?x) (ma-kuzi ?x) (ma-kvetiny ?x) (ma-kokosy ?x)
 (ma-pochybne-znamosti ?x) (ma-dobre-znamosti ?x) (seznamen-s-paseraky ?x)
 (oslnil-zenu ?x) (ma-kokain ?x)
 (kapitan ?x)
 (vyhral ?x)
    )

    (:action pit-alkohol-nalada
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
            (ma-alkohol ?who)
            (not (vnalade ?who))
            (not (opily ?who))
    )
        :effect (and
            (vnalade ?who)
	    (not (ma-alkohol ?who))
        )
    )

    (:action pit-alkohol-opilost
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
            (ma-alkohol ?who)
            (vnalade ?who)
            (not (opily ?who))
    )
        :effect (and
            (opily ?who)
	    (not (ma-alkohol ?who))
        )
    )

    (:action pit-alkohol-zavisly
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
            (ma-alkohol ?who)
            (vnalade ?who)
            (opily ?who)
            (not (zavisly ?who))
    )
        :effect (and
            (zavisly ?who)
	    (not (ma-alkohol ?who))
        )
    )

    (:action kacet-drevo
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
	(or
            (je-ostrov ?kde)
            (je-les ?kde)
	)
	    (nachazi ?who ?kde)
            (not (ma-drevo ?who))
    )
        :effect (and
            (ma-drevo ?who)
        )
    )

    (:action postavit-clun
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
            (ma-drevo ?who)
            (not (ma-clun ?who))
    )
        :effect (and
            (ma-clun ?who)
        )
    )

    (:action postavit-fregatu
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
            (ma-clun ?who)
            (ma-drevo ?who)
            (ma-zrnko ?who)
            (not (ma-fregatu ?who))
    )
        :effect (and
            (ma-fregatu ?who)
        )
    )

    (:action postavit-karavelu
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
            (ma-clun ?who)
            (ma-drevo ?who)
            (ma-mince ?who)
            (not (ma-karavelu ?who))
    )
        :effect (and
            (ma-karavelu ?who)
        )
    )

    (:action ziskat-mapku
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-les ?kde)
	    (nachazi ?who ?kde)
            (ma-alkohol ?who)
            (not (ma-mapku ?who))
    )
        :effect (and
            (ma-mapku ?who)
	    (ma-pochybne-znamosti ?who)
            (not (ma-alkohol ?who))
        )
    )

    (:action porvat-se-z-medvedem
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-les ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
             (porvan-z-mevedem ?who)
	     (ma-kuzi ?who)
        )
    )

    (:action trhat-kvetiny
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-les ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
             (ma-kvetiny ?who)
        )
    )

    (:action ukrast-clun
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-reka ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
            (ma-clun ?who)
            (je-v-tr-rejstriku ?who)
        )
    )

    (:action ryzovat-zlato
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-reka ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
            (ma-zrnko ?who)
        )
    )

    (:action vystrizlivet-reka
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-reka ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
            (not (vnalade ?who))
            (not (opily ?who))
        )
    )




    (:action pracovat
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
            (ma-zrnko ?who)
        )
    )

    (:action prodat-kuzi
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
            (ma-kuzi ?who)
    )
        :effect (and
            (ma-mince ?who)
            (not (ma-kuzi ?who))
        )
    )

    (:action prodat-kokosy
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
            (ma-kokosy ?who)
    )
        :effect (and
            (ma-mince ?who)
            (not (ma-kokosy ?who))
        )
    )

    (:action seznamit-se-s-paseraky
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
            (ma-cihlu ?who)
            (ma-pochybne-znamosti ?who)
    )
        :effect (and
            (seznamen-s-paseraky ?who)
        )
    )

    (:action koupit-alkohol
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-hospoda ?kde)
	    (nachazi ?who ?kde)
            (ma-zrnko ?who)
    )
        :effect (and
            (ma-alkohol ?who)
            (not (ma-zrnko ?who))
        )
    )

    (:action zaplatit-rundu
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-hospoda ?kde)
	    (nachazi ?who ?kde)
            (ma-mince ?who)
    )
        :effect (and
            (ma-dobre-znamosti ?who)
            (not (ma-mince ?who))
        )
    )

    (:action zocelit-v-bitce
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-hospoda ?kde)
	    (nachazi ?who ?kde)
            (vnalade ?who)
    )
        :effect (and
            (byl-v-bitce ?who)
        )
    )

    (:action stradat
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-mesto ?kde)
	    (nachazi ?who ?kde)
            (ma-zrnko ?who)
    )
        :effect (and
            (ma-mince ?who)
            (ma-dobre-znamosti ?who)
            (not (ma-zrnko ?who))
        )
    )

    (:action investovat
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-mesto ?kde)
	    (nachazi ?who ?kde)
            (ma-mince ?who)
    )
        :effect (and
            (ma-cihlu ?who)
            (ma-dobre-znamosti ?who)
            (not (ma-mince ?who))
        )
    )

    (:action pustit-do-zlodejny
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-mesto ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
            (ma-mince ?who)
            (je-v-tr-rejstriku ?who)
        )
    )

    (:action koupit-odpustek
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-mesto ?kde)
	    (nachazi ?who ?kde)
	    (ma-mince ?who)
    )
        :effect (and
            (not (ma-mince ?who))
            (not (je-v-tr-rejstriku ?who))
        )
    )

    (:action prospesne-prace
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-mesto ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
            (opily ?who)
            (not (je-v-tr-rejstriku ?who))
        )
    )

    (:action stat-kapitanem
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
	    (ma-mince ?who)
            (not (je-v-tr-rejstriku ?who))
    )
        :effect (and
            (not (ma-mince ?who))
	    (kapitan ?who)
        )
    )

    (:action piraty-rozbit-se
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-more ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
		(not (ma-fregatu ?who))
		(not (ma-karavelu ?who))
        )
    )

    (:action piraty-porazit
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-more ?kde)
	    (nachazi ?who ?kde)
	    (ma-karavelu ?who)
	    (or (porvan-z-mevedem ?who) 
		(byl-v-bitce ?who)
	    )
    )
        :effect (and
		(ma-fregatu ?who)
		(ma-karavelu ?who)
		(ma-clun ?who) 
		(ma-zrnko ?who)
		(ma-mince ?who) 
		(ma-cihlu ?who)
		(porazil-piraty ?who)
        )
    )

    (:action piraty-pridat-se
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-more ?kde)
	    (nachazi ?who ?kde)
	    (ma-pochybne-znamosti ?who)
    )
        :effect (and
            (opily ?who)
        )
    )

    (:action ledova-koupel
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-more ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
            (not (vnalade ?who))
            (not (opily ?who))
        )
    )

    (:action oslnit-zenu
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-majak ?kde)
	    (nachazi ?who ?kde)
		(or
			(porazil-piraty ?who)
			(porvan-z-mevedem ?who) 
			(kapitan ?who) 
		)
    )
        :effect (and
            (oslnil-zenu ?who)
        )
    )

    (:action nasbirat-kokosy
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-ostrov ?kde)
	    (nachazi ?who ?kde)
    )
        :effect (and
		(ma-kokosy ?who)
        )
    )

    (:action najit-kokain
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-ostrov ?kde)
	    (nachazi ?who ?kde)
	    (ma-mapku ?who)
    )
        :effect (and
		(ma-kokain ?who)
        )
    )

    (:action ozenit-se
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-ostrov ?kde)
	    (nachazi ?who ?kde)
            (oslnil-zenu ?who)
	    (ma-cihlu ?who)
	    (ma-perlu ?who)
	    (ma-kvetiny ?who)
	    (ma-dobre-znamosti ?who)
	    (not (je-v-tr-rejstriku ?who))
	    (not (opily ?who))
	    (not (zavisly ?who))
    )
        :effect (and
		(zenat ?who)
        )
    )

    (:action stat-admiralem
        :parameters (?who ?kde)
        :precondition (and
            (je-herec ?who)
            (je-akademie ?kde)
	    (nachazi ?who ?kde)
            (kapitan ?who)
	    (porazil-piraty ?who) 
	    (not (opily ?who))
	    (not (zavisly ?who))
	    (not (vnalade ?who))
    )
        :effect (and
		(admiral ?who)
        )
    )

    (:action stat-zavislym-na-kokainu
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
            (ma-kokain ?who)
	    (zavisly ?who)
	    (ma-fregatu ?who)
	    (ma-cihlu ?who)
	    (seznamen-s-paseraky ?who)
    )
        :effect (and
		(kokain ?who)
        )
    )

    (:action vyhrat
        :parameters (?who)
        :precondition (and
            (je-herec ?who)
	    (or 
		(kokain ?who)
		(admiral ?who)
		(zenat ?who)
	     )
    )
        :effect (and
		(vyhral ?who)
        )
    )













    (:action pristav-reka
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
            (je-reka ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action reka-pristav
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (je-reka ?kde)
	    (nachazi ?who ?kde)
            (je-pristav ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action reka-les
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (je-reka ?kde)
	    (nachazi ?who ?kde)
            (je-les ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action les-reka
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (je-les ?kde)
	    (nachazi ?who ?kde)
            (je-reka ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action pristav-hospoda
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
            (je-hospoda ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action hospoda-pristav
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (je-hospoda ?kde)
	    (nachazi ?who ?kde)
            (je-pristav ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action pristav-mesto
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
            (je-mesto ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action mesto-pristav
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (je-mesto ?kde)
	    (nachazi ?who ?kde)
            (je-pristav ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action pristav-majak
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (or 
	        (ma-fregatu ?who)
                (ma-karavelu ?who)
                (ma-clun ?who)
	    )
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
            (je-majak ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action majak-pristav
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (or 
	        (ma-fregatu ?who)
                (ma-karavelu ?who)
                (ma-clun ?who)
	    )
            (je-majak ?kde)
	    (nachazi ?who ?kde)
            (je-pristav ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action majak-more
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (or 
	        (ma-fregatu ?who)
                (ma-karavelu ?who)
                (ma-clun ?who)
	    )
            (je-majak ?kde)
	    (nachazi ?who ?kde)
            (je-more ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action more-majak
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (or 
	        (ma-fregatu ?who)
                (ma-karavelu ?who)
                (ma-clun ?who)
	    )
            (je-more ?kde)
	    (nachazi ?who ?kde)
            (je-majak ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action more-pristav
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (or 
	        (ma-fregatu ?who)
                (ma-karavelu ?who)
                (ma-clun ?who)
	    )
            (je-more ?kde)
	    (nachazi ?who ?kde)
            (je-pristav ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action pristav-more
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (or 
	        (ma-fregatu ?who)
                (ma-karavelu ?who)
                (ma-clun ?who)
	    )
            (je-pristav ?kde)
	    (nachazi ?who ?kde)
            (je-more ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action more-ostrov
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (or 
	        (ma-fregatu ?who)
                (ma-karavelu ?who)
                (ma-clun ?who)
	    )
            (je-more ?kde)
	    (nachazi ?who ?kde)
            (je-ostrov ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )

    (:action ostrov-more
        :parameters (?who ?kde ?kam)
        :precondition (and
            (je-herec ?who)
            (or 
	        (ma-fregatu ?who)
                (ma-karavelu ?who)
                (ma-clun ?who)
	    )
            (je-ostrov ?kde)
	    (nachazi ?who ?kde)
            (je-more ?kam)
    )
        :effect (and
            (nachazi ?who ?kam)
            (not (nachazi ?who ?kde))
        )
    )
)
